# 404 Alarm Clock

Simple Alarm Clock 404 page using jQuery and momentjs.

## Getting Started

Copy files to web server and link 404 to 404.html (minified version)


## Built With

## JS
* jQuery 3.2.1
* MomentJS 2.19.1

### Fonts
* DS-Digital
* ASAP

## Contributing

## Authors

* **Brent Patrochn** - *Initial work* - [Modev Media](https://modevmedia.com)

## License

This project is licensed under the MIT License 
